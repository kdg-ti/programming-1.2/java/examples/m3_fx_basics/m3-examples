package removeif;

import java.util.*;

public class Eraser {

  public static void main(String[] args) {
    Collection<String> band = new ArrayList<>(List.of("Dave", "Dee", "Dozy",
        "Beaky", "Mick", "Titch"));
    band.removeIf(member -> member.endsWith("y"));
    System.out.println("Members not ending with y: "+ band);
  }

}
