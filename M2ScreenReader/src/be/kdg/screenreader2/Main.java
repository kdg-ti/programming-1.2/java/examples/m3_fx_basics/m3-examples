package be.kdg.screenreader2; /**
 * Author: Jan de Rijke
 */

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Screen Reader");
		primaryStage.setWidth(300);
		primaryStage.setHeight(450);
		primaryStage.show();
	}
}
