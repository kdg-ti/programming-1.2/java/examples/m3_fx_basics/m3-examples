package be.kdg.screenreader5;

import be.kdg.screenreader5.model.ScreenReader;

/**
 * Author: Jan de Rijke
 */
public class ModelTest {
	public static void main(String[] args) {
		ScreenReader reader = new ScreenReader();
		reader.setText("I'm sorry Dave. I'm afraid I can't do that");
		reader.readAloud();
	}
}
