package be.kdg.screenreader5.view;

import be.kdg.screenreader5.model.ScreenReader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Author: Jan de Rijke
 */
public class ScreenReaderPresenter {

	private ScreenReader model;
	private ScreenReaderView view;

	public ScreenReaderPresenter(
		ScreenReader model,ScreenReaderView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void updateView() {
		view.getTextArea().setText(model.getText());
	}

	private void addEventHandlers() {
		view.getReadButton().setOnAction( event -> {
			model.setText(view.getTextArea().getText());
			model.readAloud();
			// updateView is not needed here
			// but if the view has to be updated with model changes
			// this is a good practice
			updateView();
		});
	}
}
