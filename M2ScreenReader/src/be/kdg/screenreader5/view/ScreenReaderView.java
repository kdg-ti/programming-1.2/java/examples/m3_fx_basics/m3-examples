package be.kdg.screenreader5.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

/**
 * Author: Jan de Rijke
 */
public class ScreenReaderView extends BorderPane {
	private Button readButton;
	private TextArea textArea;

	public ScreenReaderView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		setCenter(textArea);
		setBottom(readButton);
		setAlignment(readButton, Pos.CENTER);
		setMargin(readButton,new Insets(10, 10, 10, 10));
	}

	private void initialiseNodes() {
		readButton = new Button("Read Aloud");
		textArea = new TextArea("Enter text to read");
	}

	Button getReadButton() {
		return readButton;
	}

	TextArea getTextArea() {
		return textArea;
	}
}
