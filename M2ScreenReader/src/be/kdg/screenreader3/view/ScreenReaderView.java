package be.kdg.screenreader3.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

/**
 * Author: Jan de Rijke
 */
public class ScreenReaderView extends BorderPane {
	private Button readButton;
	private TextArea textArea;

	public ScreenReaderView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		readButton = new Button("Read Aloud");
		textArea = new TextArea("Enter text to read");
	}
	private void layoutNodes() {
		setCenter(textArea);
		setBottom(readButton);
		BorderPane.setAlignment(readButton, Pos.CENTER);
		BorderPane.setMargin(readButton,new Insets(10, 10, 10, 10));
	}



}
