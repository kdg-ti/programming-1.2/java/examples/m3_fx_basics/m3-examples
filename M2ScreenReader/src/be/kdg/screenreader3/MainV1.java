package be.kdg.screenreader3; /**
 * Author: Jan de Rijke
 */

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainV1 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Button readButton = new Button("Read Aloud");
		TextArea textArea = new TextArea("Enter text to read");
		BorderPane root = new BorderPane();
		root.setCenter(textArea);
		root.setBottom(readButton);
		BorderPane.setAlignment(readButton, Pos.CENTER);
		BorderPane.setMargin(readButton,
			new Insets(10, 10, 10, 10));
		primaryStage.setTitle("Screen Reader");
		primaryStage.setWidth(300);
		primaryStage.setHeight(450);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

}
