To run the examples, make sure to:
- add JavaFX libraries
- configure the Main class run configuration with JavaFX options
- add the FreeTTS libraries (for the screenreader example)